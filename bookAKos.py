from cgitb import text
from distutils.log import debug
from lib2to3.pgen2 import driver
from multiprocessing.connection import wait
from select import select
from time import sleep
from selenium import webdriver
import random
import string
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import loginAsPencariKos

browser = loginAsPencariKos.browser

#CallingTheLoginFunction
loginAsPencariKos.login()

#MovingToTheKostPage
browser.get("https://mamikos.com/room/kost-kabupaten-simeulue-kost-campur-eksklusif-kos-agen-duo-tenant-1#/")
buttonAjukanSewa = browser.find_element_by_css_selector("section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container.--sticky div.booking-card section.booking-card__booking > button.mami-button.booking-card__booking-action.track_request_booking.mami-button--primary.mami-button--large.mami-button--block")
browser.execute_script("window.scrollTo(0,document.body.scrollHeight)")

#WaitingForTheTutorialPopUp
element = WebDriverWait(browser, 20).until(
EC.element_to_be_clickable((By.CLASS_NAME, "cta-wrapper")))

#FinishingTheTutorial
buttonsayaMengerti = browser.find_element_by_class_name("cta-wrapper")
for i in range(6):
    ActionChains(browser).move_to_element(buttonsayaMengerti).click(buttonsayaMengerti).perform()

#ChoosingTheStartingDateAndPaymentPeriod
ActionChains(browser).move_to_element(buttonAjukanSewa).click(buttonAjukanSewa).perform()
exampleDate = browser.find_element_by_css_selector('section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container div.booking-card section.booking-card__info div.booking-card__info-select section.booking-input-checkin.booking-card__info-select-date.is-open div.booking-input-checkin-content.booking-input-checkin__date.is-absolute section.booking-input-checkin-content__date-picker:nth-child(2) div.bg-c-datepicker div.vdp-datepicker div.vdp-datepicker__calendar.inline:nth-child(2) div.picker-view div:nth-child(2) div.date-wrapper div.date-wrapper__cell-parent:nth-child(42) > span.cell.day.muted.weekend.sun')
ActionChains(browser).move_to_element(exampleDate).click(exampleDate).perform()
paymentPeriodPerBulan = browser.find_element_by_css_selector('section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container.--sticky div.booking-card section.booking-card__info div.booking-card__info-select div.booking-rent-type.--open div.booking-rent-type__options div.booking-rent-type__options-item > div.mami-radio')
ActionChains(browser).move_to_element(paymentPeriodPerBulan).click(paymentPeriodPerBulan).perform()

#VerifyingTheMothlyRateAndServiceFee
hargaSewaPerBulan = browser.find_element_by_css_selector('section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container.--sticky div.booking-card section.booking-card__booking div.booking-card-prices.booking-card__booking-prices div.booking-card-prices__detail div.price-estimation div.detail-kost-price-item:nth-child(1) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--body-1').text
hargaSewaPerBulanNoCurrency = hargaSewaPerBulan.replace('Rp','')
hargaSewaPerBulanClean = hargaSewaPerBulanNoCurrency.replace('.','')
biayaLayanan = browser.find_element_by_css_selector('section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container.--sticky div.booking-card section.booking-card__booking div.booking-card-prices.booking-card__booking-prices div.booking-card-prices__detail div.price-estimation div.detail-kost-price-item:nth-child(2) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--body-1').text
biayaLayananNoCurrency = biayaLayanan.replace('Rp','')
biayaLayananClean = biayaLayananNoCurrency.replace('.','')
totalPembayaranPertama = browser.find_element_by_css_selector('section.section-detail div.content-container:nth-child(3) div.col-md-5.hidden-xs.hidden-sm.card-container.--sticky div.booking-card section.booking-card__booking div.booking-card-prices.booking-card__booking-prices div.booking-card-prices__detail div.detail-kost-price-item > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--heading-6').text
totalPembayaranPertamaNoCurrency = totalPembayaranPertama.replace('Rp','')
totalPembayaranPertamaClean = totalPembayaranPertamaNoCurrency.replace('.','')
assert (int(hargaSewaPerBulanClean) + int(biayaLayananClean)) == int(totalPembayaranPertamaClean)

#GoingToTheNextStep
ActionChains(browser).move_to_element(buttonAjukanSewa).click(buttonAjukanSewa).perform()

#VerifyingData
hargaSewaPerBulanB = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-steps-container.col-xs-12.col-sm-6.col-md-7 div.booking-input-price.booking-steps__router div.booking-input-price__radio div.container-radio-duration div.rent-type-group div.mami-radio.selected div.rent-type:nth-child(3) div.price-value > strong:nth-child(1)').text.replace('Rp','').replace('.','')
buttonSelanjutnya = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-steps-container.col-xs-12.col-sm-6.col-md-7 div.booking-action-buttons.booking-steps__button > button.bg-c-button.track_next_booking.bg-c-button--primary.bg-c-button--lg')
ActionChains(browser).move_to_element(buttonSelanjutnya).click(buttonSelanjutnya).perform()
element = WebDriverWait(browser, 20).until(
EC.element_to_be_clickable((By.CSS_SELECTOR, 'div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-content div.booking-summary-price:nth-child(7) div.detail-kost-price-item:nth-child(2) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--body-1')))
hargaSewaPerBulanC = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-content div.booking-summary-price:nth-child(7) div.detail-kost-price-item:nth-child(2) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--body-1').text.replace('Rp','').replace('.','')
biayaLayananB = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-content div.booking-summary-price:nth-child(7) div.detail-kost-price-item:nth-child(3) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--body-1').text.replace('Rp','').replace('.','')
totalPembayaranPertamaB = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-content div.booking-summary-price:nth-child(7) div.detail-kost-price-item:nth-child(5) > p.detail-kost-price-item__amount.bg-c-text.bg-c-text--heading-6').text.replace('Rp','').replace('.','')
assert int(hargaSewaPerBulanB) == int(hargaSewaPerBulanC)
assert int(hargaSewaPerBulanB) + int(biayaLayananB) == int(totalPembayaranPertamaB)

#SubmittingData
checkbox = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-action div.booking-summary__tnc div.bg-c-checkbox label:nth-child(1) > span.bg-c-checkbox__icon')
ActionChains(browser).move_to_element(checkbox).click(checkbox).perform()
buttonAjukanSewaFinal = browser.find_element_by_css_selector('div.booking-content.container:nth-child(2) div.booking-wrapper.col-xs-12 div.booking-summary-container.hidden-xs.col-sm-6.col-md-5 div.booking-summary div.booking-summary__booking-action > button.bg-c-button.bg-c-button--primary.bg-c-button--lg.bg-c-button--block')
ActionChains(browser).move_to_element(buttonAjukanSewaFinal).click(buttonAjukanSewaFinal).perform()

#WaitingForConfirmationOfSuccess
element = WebDriverWait(browser, 20).until(
EC.element_to_be_clickable((By.CLASS_NAME, "booking-success__title")))

browser.quit()