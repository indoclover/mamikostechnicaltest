from distutils.log import debug
from multiprocessing.connection import wait
from select import select
from time import sleep
from selenium import webdriver
import random
import string
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

browser = webdriver.Chrome()
browser.get("https://mamikos.com/login-pencari")

def login():

    element = WebDriverWait(browser, 20).until(
    EC.element_to_be_clickable((By.XPATH, "//body/div[@id='app']/div[1]/div[3]/div[1]/div[3]/div[1]/form[1]/fieldset[1]/div[1]/input[1]")))

    #ListofElements
    nomorHandphone = browser.find_element_by_xpath("//body/div[@id='app']/div[1]/div[3]/div[1]/div[3]/div[1]/form[1]/fieldset[1]/div[1]/input[1]")
    password = browser.find_element_by_xpath("//body/div[@id='app']/div[1]/div[3]/div[1]/div[3]/div[1]/form[1]/fieldset[1]/div[2]/input[1]")
    buttonLogin = browser.find_element_by_css_selector("div.auth-wrapper div.auth-wrapper__body div.login-wrapper.auth-desktop-container.bg-c-card.bg-c-card--lined.bg-c-card--md.bg-c-card--light.auth-step-card div.auth-step-card__body div.form-login form.input-form fieldset:nth-child(1) button.btn.btn-primary.btn-mamigreen.login-button.track-login-tenant:nth-child(3) > span.track-login-tenant")

    #FillingTheForm
    ActionChains(browser).move_to_element(nomorHandphone).click(nomorHandphone).perform()
    ActionChains(browser).move_to_element(nomorHandphone).send_keys("081517231313").perform()
    ActionChains(browser).move_to_element(password).click(password).perform()
    ActionChains(browser).move_to_element(password).send_keys("`0An2254").perform()
    ActionChains(browser).move_to_element(buttonLogin).click(buttonLogin).perform()

    element = WebDriverWait(browser, 20).until(
    EC.element_to_be_clickable((By.CSS_SELECTOR, "div.nav-section.nav-fixed:nth-child(1) div.nav-main-container div.nav-container nav.nav-main-navbar ul.nav-main-ul:nth-child(1) div.user-profile-dropdown.bg-c-dropdown:nth-child(6) div.bg-c-dropdown__trigger div.user-profile-dropdown__trigger > img.user-profil-img")))